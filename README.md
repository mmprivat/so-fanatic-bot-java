# SoFanaticBot 1.1

This is my Stackoverflow Fanatic Badge Bot. If compiled, it produces a runnable JAR file. When startet it will run for 101 consecutive days of logins to Stackoverflow.

## Prerequisites

* Maven
* Java 8+
* Gecko- or Chromedriver
* Either configured Gecko- / Chromedriver via System Properties or put it in the default dir with default name  `./browserdriver`

## Getting Started

To run this little bot, you have to fulfilll all prerequisites and execute following commands:

```bash
# mvn package
# cd target
# java -jar so-fanatic-bot-1.0-SNAPSHOT-jar-with-dependencies.jar -D<userName> -D<password> [-D<loginType>] [-D<browser>]
```

To see the possible values for `loginType` and `browser` you can execute the jar file without any parameters.

Alternatively to give all the parameters, you can place them into a JSON file and give this as single parameter. You can find an example of how this could look (here)[./src/main/resources/example.config.json].

## About the project

### Versioning

We use MAIN.MINOR number for versioning where as MAIN usually means big / breaking changes and MINOR usually means small / non breaking changes. For the version number, see at the top header or in the [tags on this repository](https://bitbucket.org/mmprivat/wordpress/downloads/?tab=tags).

### Author(s)

* **Manuel Manhart** - *Initial work*

### License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

### Contributing

If you want to contribute, contact the author or create a pull request.

### Changelog

__1.0-SNAPSHOT__

* Initial build

## Known issues

  * Currently only supports login via google
  * Only tested with firefox and firefox in headless mode

## Sources

  * https://github.com/mozilla/geckodriver/releases/tag/v0.26.0
