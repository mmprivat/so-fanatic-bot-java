package space.manhart.so.fanatic.bot;

import org.junit.Assert;
import org.junit.Test;

public class JsonConfigReaderTest {

    @Test
    public void testUserNameForCheck() {
    	String configFile = "./src/main/resources/example.config.json";
    	Config config = JsonConfigReader.readConfig(configFile);

    	String expected = "Manuel";
		String actual = config.getUserNameForCheck();
		Assert.assertEquals(expected, actual);
    }

    @Test
    public void testLoginName() {
    	String configFile = "./src/main/resources/example.config.json";
    	Config config = JsonConfigReader.readConfig(configFile);

    	String expected = "manuel@gmail.com";
		String actual = config.getLoginName();
		Assert.assertEquals(expected, actual);
    }

    @Test
    public void testDriverPath() {
    	String configFile = "./src/main/resources/example.config.json";
    	Config config = JsonConfigReader.readConfig(configFile);

    	String expected = "/home/manuel/geckodriver";
		String actual = config.getBrowserConfig().getDriverPath();
		Assert.assertEquals(expected, actual);
    }
}