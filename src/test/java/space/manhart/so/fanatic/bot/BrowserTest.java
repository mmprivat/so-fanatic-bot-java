package space.manhart.so.fanatic.bot;

import org.junit.*;

public class BrowserTest {

    @Test
    public void testChrome() {
		String name = "cHrOmE";
		Browser expected = Browser.CHROME;
		Browser actual = Browser.parse(name);
		Assert.assertEquals(expected, actual);
    }

    @Test
    public void testHeadless() {
		String name = "cHrOmEhEaDlEsS";
		Browser expected = Browser.CHROME;
		Browser actual = Browser.parse(name);
		Assert.assertEquals(expected, actual);
    }
}