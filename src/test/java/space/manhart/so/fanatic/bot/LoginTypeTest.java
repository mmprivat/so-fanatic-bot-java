package space.manhart.so.fanatic.bot;

import org.junit.*;

public class LoginTypeTest {

    @Test
    public void testGoogle() {
		String name = "gOoGlE";
		LoginType expected = LoginType.GOOGLE;
		LoginType actual = LoginType.parse(name);
		Assert.assertEquals(expected, actual);
    }
}