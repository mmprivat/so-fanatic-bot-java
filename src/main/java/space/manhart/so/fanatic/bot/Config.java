package space.manhart.so.fanatic.bot;

public class Config {
	private String loginName;
	private String userNameForCheck;
	private String password;
	private BrowserConfig browserConfig;
	private LoginType loginType;

	public Config() {
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getUserNameForCheck() {
		return userNameForCheck;
	}

	public void setUserNameForCheck(String userNameForCheck) {
		this.userNameForCheck = userNameForCheck;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public BrowserConfig getBrowserConfig() {
		return browserConfig;
	}

	public void setBrowserConfig(BrowserConfig browserConfig) {
		this.browserConfig = browserConfig;
	}

	public LoginType getLoginType() {
		return loginType;
	}

	public void setLoginType(LoginType loginType) {
		this.loginType = loginType;
	}

	public boolean validate() {
		if (isEmpty(userNameForCheck)) {
			return false;
		}
		if (isEmpty(loginName)) {
			return false;
		}
		if (isEmpty(password)) {
			return false;
		}
		if (browserConfig == null || browserConfig.getBrowser() == null) {
			return false;
		}
		return true;
	}

	private boolean isEmpty(String check) {
		return (check == null || check.length() == 0);
	}

	public static class BrowserConfig {
		private Browser browser;
		private boolean headless;
		private String profilePath;
		private String driverPath;

		public BrowserConfig() {
			headless = false;
		}
		
		public Browser getBrowser() {
			return browser;
		}

		public void setBrowser(Browser browser) {
			this.browser = browser;
		}

		public boolean isHeadless() {
			return headless;
		}

		public void setHeadless(boolean headless) {
			this.headless = headless;
		}

		public String getProfilePath() {
			return profilePath;
		}

		public void setProfilePath(String profilePath) {
			this.profilePath = profilePath;
		}

		public void parseHeadless(String arg) {
			if (arg.toLowerCase().endsWith("headless")) {
				setHeadless(true);
			}
		}

		public String getDriverPath() {
			if (driverPath == null) {
				return "./browserdriver";
			}
			return driverPath;
		}

		public void setDriverPath(String driverPath) {
			this.driverPath = driverPath;
		}
	}
}