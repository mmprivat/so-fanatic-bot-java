package space.manhart.so.fanatic.bot;

public enum LoginType {
	GOOGLE, GITHUB, FACEBOOK, DIRECT;

	public static LoginType parse(String name) {
		return parse(name, null);
	}

	public static LoginType parse(String name, LoginType defaultValue) {
		for (LoginType t : values()) {
			if (t.name().toLowerCase().equals(name.toLowerCase())) {
				return t;
			}
		}
		return defaultValue;
	}
}