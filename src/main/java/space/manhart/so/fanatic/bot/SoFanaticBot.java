package space.manhart.so.fanatic.bot;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimerTask;

import space.manhart.so.fanatic.bot.Config.BrowserConfig;

/**
 * 
 * @author Manuel M.
 */

public class SoFanaticBot extends TimerTask {
	private static int MAX_DAYS = 100;
	private static int SLEEP_TIME = 1000 * 60 * 60 * 6;
	private int days = 0, lastLoginDay = -1;

	Config config = new Config();

	public SoFanaticBot() {
	}

	public void run() {
		while ( !hasFinished() ) {
			checkLogin();
			try {
				Thread.sleep(SLEEP_TIME);
			} catch (Throwable t) {
			}
		}
	}

	public void checkLogin() {
		int today = new Integer(new SimpleDateFormat("yyyyMMdd").format(new Date()));

		if (lastLoginDay < today) {
			try {
				SoLogin l = new SoLogin(this.config);
				if (l.login(today)) {
					lastLoginDay = today;
					days++;
				}
			} catch (Throwable t) {
				System.err.println(t);
			}
		}
	}

	public boolean hasFinished() {
		return days > MAX_DAYS;
	}

	public void setConfig(String args[]) {
		try {
			if (args.length == 1) {
				System.out.println("Reading config from file");
				// read config from json file
				config = JsonConfigReader.readConfig(args[0]);
			} else if (args.length < 2 || args.length > 4) {
				System.out.println("Reading config from parameters");
				// read config from params
				LoginType loginType = LoginType.GOOGLE;
				Browser browser = Browser.FIREFOX;
				BrowserConfig browserConfig = new BrowserConfig();
				if (args.length > 2) {
					loginType = LoginType.parse(args[2], loginType);
					browser = Browser.parse(args[2], browser);
					browserConfig.parseHeadless(args[2]);
				}
				if (args.length > 3) {
					loginType = LoginType.parse(args[3], loginType);
					browser = Browser.parse(args[3], browser);
					browserConfig.parseHeadless(args[3]);
				}
				config.setLoginType(loginType);
				browserConfig.setBrowser(browser);
				config.setBrowserConfig(browserConfig);
			}
	
			// validate config
			if (!config.validate()) {
				printHelp();
			}
		} catch (Throwable t) {
			t.printStackTrace();
			printHelp();
		}
	}

	public void printHelp() {
		System.out.println("STACKOVERFLOW FANATIC BOT");
		System.out.println("-------------------------");
		System.out.println("");
		System.out.println("USAGE:");
		System.out.println("    java -jar so-fanatic-bot-1.0-SNAPSHOT-jar-with-dependencies.jar PARAMS");
		System.out.println("");
		System.out.println("Config via params:");
		System.out.println("You have to pass at least 2 arguments:");
		System.out.println("    loginName (your login)");
		System.out.println("    userName (your username as shown on profile page)");
		System.out.println("    password (your password)");
		System.out.println("optionally you can pass 2 more arguments:");
		System.out.println("    loginType (GOOGLE (default), DIRECT, FACEBOOK, GITHUB)");
		System.out.println("    browser (FIREFOX (default), CHROME, FIREFOXHEADLESS, CHROMEHEADLESS)");
		System.out.println("");
		System.out.println("Config via json file:");
		System.out.println("Pass as single argument the JSON file path, the json should look like this:");
		System.out.println("{");
		System.out.println("	\"userNameForCheck\": \"yourUsernameAsShownOnProfilePage\",");
		System.out.println("	\"loginName\": \"yourLoginname\",");
		System.out.println("	\"password\": \"yourPassword\",");
		System.out.println("	\"browser\": {");
		System.out.println("		\"name\": \"FIREFOX\",");
		System.out.println("		\"headless\": true,");
		System.out.println("		\"profilePath\": \"/path/to/profile\"");
		System.out.println("		\"driverPath\": \"/path/to/driver\"");
		System.out.println("	},");
		System.out.println("	\"loginType\": \"GOOGLE\"");
		System.out.println("}");
		System.exit(0);
	}

	public static void main(String args[]) throws InterruptedException {
		SoFanaticBot bot = new SoFanaticBot();
		bot.setConfig(args);
		bot.run();
	}
}
