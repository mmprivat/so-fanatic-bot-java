package space.manhart.so.fanatic.bot;

import java.io.File;
import java.io.IOException;

import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonConfigReader {

	public static Config readConfig(String configFile) {
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			return objectMapper.readValue(new File(configFile), Config.class);
		} catch (IOException e) {
			e.printStackTrace();
		}
		throw new RuntimeException("Could not read config file '" + configFile + "'");
	}

}