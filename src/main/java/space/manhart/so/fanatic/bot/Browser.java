package space.manhart.so.fanatic.bot;

public enum Browser {
	CHROME, FIREFOX;

	private Browser() {
	}

	public static Browser parse(String name) {
		return parse(name, null);
	}

	public static Browser parse(String name, Browser defaultValue) {
		for (Browser t : values()) {
			if (name.toLowerCase().startsWith(t.name().toLowerCase())) {
				return t;
			}
		}
		return defaultValue;
	}
}