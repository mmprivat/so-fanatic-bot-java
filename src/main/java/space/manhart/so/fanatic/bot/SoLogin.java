package space.manhart.so.fanatic.bot;

import java.io.File;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;

import space.manhart.so.fanatic.bot.Config.BrowserConfig;

public class SoLogin  {
	private Config config;

	public SoLogin(Config config) {
		this.config = config;
	}

	public WebDriver getDriver() {
		BrowserConfig bc = config.getBrowserConfig();
		switch (bc.getBrowser()) {
			case CHROME:
				ChromeOptions chOptions = new ChromeOptions();
				if (System.getProperty("webdriver.chrome.driver") == null) {
					System.setProperty("webdriver.chrome.driver", bc.getDriverPath());
				}
				if (bc.isHeadless()) {
					chOptions.setHeadless(true);
				}
				return new ChromeDriver(chOptions);
			case FIREFOX:
				FirefoxOptions ffOptions = new FirefoxOptions();
				if (System.getProperty("webdriver.firefox.driver") == null) {
					System.setProperty("webdriver.gecko.driver", bc.getDriverPath());
				}
				if (bc.isHeadless()) {
					ffOptions.setHeadless(true);
				}
				if (bc.getProfilePath() != null) {
					ffOptions.setProfile(new FirefoxProfile(new File(bc.getProfilePath())));
				}
				return new FirefoxDriver(ffOptions);
			default:
				System.out.println("Browser " + bc.getBrowser() + " is not supported.");
				System.exit(1);
				return null;
		}
	}

	public boolean login(int today) throws InterruptedException {
		String loginName = config.getLoginName();
		String userNameForCheck = config.getUserNameForCheck();
		String password = config.getPassword();

		System.out.println("***********************");
		System.out.println("Doing login for " + today);

        // Create a new instance of the html unit driver
        // Notice that the remainder of the code relies on the interface, 
        // not the implementation.
        WebDriver driver = getDriver();

        // And now use this to visit Google
        driver.get("https://stackoverflow.com/");

        System.out.println("Page title is: " + driver.getTitle());
        // Open Login Page
        WebElement element = driver.findElement(By.className("login-link"));
        element.click();

        System.out.println("Page title is: " + driver.getTitle());
        // Select Google Login
		switch (config.getLoginType()) {
			case GOOGLE:
				loginViaGoogle(driver, loginName, password);
				break;
			default:
				System.out.println("ATM no other login method than via Google supported.");
				System.exit(1);
		}

        // Check the title of the page
		String title = driver.getTitle();
        element = driver.findElement(By.className("my-profile"));
		boolean result = false;
		if (element.getAttribute("href").contains(userNameForCheck)) {
			System.out.println(today + " - User confirmed");
			result = true;
		} else if (title.contains("Stack Overflow")) {
			System.err.println(today + " ERROR - Page is SO, User not confirmed");
		} else {
			System.err.println(today + " ERROR - Page is " + title);
		}

        driver.quit();
		System.out.println("***********************");
		return result;
    }

	private void loginViaGoogle(WebDriver driver, String loginName, String password) throws InterruptedException {
        WebElement element = driver.findElement(By.className("s-btn__google"));
        element.click();
		Thread.sleep(1000);

        element = driver.findElement(By.id("identifierId"));
        element.sendKeys(loginName);
		Thread.sleep(1000);

        element = driver.findElement(By.id("identifierNext"));
        element.click();
		Thread.sleep(1000);

        element = driver.findElement(By.name("password"));
        element.sendKeys(password);
		Thread.sleep(1000);

        element = driver.findElement(By.id("passwordNext"));
        element.click();
		Thread.sleep(1000);
	}
}